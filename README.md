# Vocabulary

This project manages the semantic vocabular for describing an electrical machine and its production process.
It uses [Protégé](https://protege.stanford.edu/) to create the descriptions.
A HTML page is hosted with Gitlab Pages (TODO).


## Repository Structure

(TODO)

## Links

- [Book: An Introduction to Ontology Engineering](https://people.cs.uct.ac.za/~mkeet/files/OEbook.pdf)

## Links to other Vocabularies

- [Search Engine for finding other vocabularies and terms](https://lov.linkeddata.es/dataset/lov/)
- [RDF Schema Compendium](https://schemapedia.com/index/)

----
- [Smart Appliances REFerence (SAREF): describes smart assets with functions, commands, states, service and profile](https://ontology.tno.nl/saref/)
- [SAREF4INMA: an extension of SAREF for the industry and manufacturing domain](https://saref.etsi.org/saref4inma/v1.1.2/)
